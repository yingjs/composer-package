<?php

namespace Yjius\aibigmodel;

class AIBigModelBuilder implements AIBigModelBuilderInterface
{
    //实例化代码
    protected $instance = null;

    /**
     * 构造函数：根据提供的参数创建并初始化相应的实例
     *
     * 本构造函数的主要作用是根据$params数组中的'driver'键值，选择不同的具体实现类
     * 这些具体实现类提供了与不同平台交互的功能本构造函数还负责处理不支持的平台调用情况
     *
     * @param array $params 包含了用于创建具体实现类实例的参数，其中包括了指定使用的驱动（平台）
     * @throws \Exception 当$params中的'driver'不匹配任何支持的平台时，抛出异常
     */
    public function __construct($params)
    {
        // 根据提供的驱动名称选择并创建对应的实例
        switch ($params['driver']) {
            case 'tongyiqianwen':
                //通义千问大模型 https://bailian.console.aliyun.com/
                $this->instance = qianwen\QianWen::getInstance($params);
                break;
            case 'doubao':
                //豆包大模型 https://www.volcengine.com/docs/82379/1399008
                $this->instance = doubao\DouBao::getInstance($params);
                break;
            case 'chatgptMetaWa':
                //chatgpt大模型
                //来自于元娲接口的封装调用MetaWa
                //其中role为system不生效、仅支持question
                $this->instance = chatgpt\ChatgptMetaWa::getInstance($params);
                break;
            case 'deepseek':
                //deepseek大模型 https://platform.deepseek.com/api_keys
                $this->instance = deepseek\DeepSeek::getInstance($params);
                break;
            default:
                // 当尝试使用不支持的驱动时抛出异常
                throw new \Exception("暂不支持其他平台调用");
        }
    }

    /**
     * 执行一次聊天操作
     *
     * 该方法用于调用实例的chatOnce方法，传递参数数组以进行聊天操作
     * 主要作用是对外提供一个接口，以便在外部调用聊天功能
     *
     * @param array $params 聊天操作所需的参数数组，包含聊天所需的各项配置和信息
     * @return mixed 聊天操作的结果，具体类型取决于chatOnce方法的实现
     */
    public function chatOnce(array $params = [], array $customerParams = [])
    {
        return $this->instance->chatOnce($params, $customerParams);
    }

    /**
     * 聊天一次性处理结果
     *
     * 该方法用于处理一次性聊天结果，通常是在聊天过程中需要进行一次性操作或响应的场景
     * 它通过调用实例的相应方法来处理聊天结果
     *
     * @param string $params 聊天处理所需的参数字符串
     *
     * @return mixed 返回处理结果，具体类型取决于实例方法的返回值
     */
    public function chatOnceDealResult(string $params = "", array $customerParams = [])
    {
        return $this->instance->chatOnceDealResult($params, $customerParams);
    }

    /**
     * Purpose: 流式输出结果
     * @param array $params
     * @param array $customerParams
     */
    public function sseChatOnce(array $params = [], callable $handlerFunction = null, array $customerParams = [])
    {
        return $this->instance->sseChatOnce($params, $handlerFunction, $customerParams);
    }

    /**
     * @return chatgpt\ChatgptMetaWa|deepseek\DeepSeek|doubao\DouBao|qianwen\QianWen
     */
    public function getInstance()
    {
        return $this->instance;
    }

}