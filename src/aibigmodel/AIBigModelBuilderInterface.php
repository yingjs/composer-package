<?php

namespace Yjius\aibigmodel;

interface AIBigModelBuilderInterface
{
    /**
     * Purpose: 单次聊天的chat调用，传入数组内容覆盖默认参数，返回结果
     * @param array $paramsArray
     * @return mixed
     */
    function chatOnce(array $paramsArray, array $customerParams);

    /**
     * Purpose: 单次聊天的chat调用结果传入，去格式输出想要的的内容
     * @param string $paramsStr
     * @return mixed
     */
    function chatOnceDealResult(string $paramsStr, array $customerParams);

}