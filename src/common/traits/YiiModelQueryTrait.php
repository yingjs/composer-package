<?php

namespace Yjius\common\traits;


trait YiiModelQueryTrait
{

    /**
     * Purpose: 保存一条数据,如果没有就新增
     * $data = ["id"=>1,"name"=>"test];
     */
    public static function saveDataNoExistAdd($data = [], $pkValue = null, $pk = "id")
    {
        $model = self::findOne([$pk => $pkValue]);
        if (empty($model)) {
            $model = new self();
        }
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        if ($model->save()) {
            return $model->attributes[$pk];
        } else {
            return false;
        }
    }

    /**
     * Purpose: 获取数据的map数据，方便获取数据格式
     */
    public static function getDataMap($where, $fields = [], $indexKey = "id", $withCacheSec = 0)
    {
        $mapInfo = [];
        $query = self::find()->select($fields)->where($where);
        if (!empty($withCacheSec)) {
            $query->cache($withCacheSec);
        }
        $data = $query->asArray()->all();
        if (!empty($data)) {
            $mapInfo = array_column($data, null, $indexKey);
        }
        return $mapInfo;
    }

    /**
     * Purpose: 每50条 批量入库
     * $data = [
     *     ["id"=>1,"name"=>"test],
     *     ["id"=>2,"name"=>"test2"]
     * ];
     */
    public static function batchInsertMain($data = [], $limitNum = 50)
    {
        $i = 0;
        $insert = $fields = [];
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                if ($key == 0) {
                    $fields = array_keys($item);
                }
                $insert[] = $item;
                $i++;
                if ($i >= $limitNum) {
                    self::batchInsertData($insert, $fields);
                    $i = 0;
                    $insert = [];
                }
            }
        }
        //剩余入库
        if (!empty($insert)) {
            self::batchInsertData($insert, $fields);
        }
        return $data;
    }

    private static function batchInsertData($insert = [], $fields = [])
    {
        if (!empty($insert) && !empty($fields)) {
            \Yii::$app->db->createCommand()->batchInsert(static::tableName(), $fields, $insert)->execute();//执行批量添加
        }
        return true;
    }

    //删除数据
    public static function deleteData($condition = '')
    {
        return self::deleteAll($condition);
    }

    //更新数据
    public static function updateData($attributes, $condition = '', $params = [])
    {
        return self::updateAll($attributes, $condition, $params);
    }

    //分页查询数据，带数量返回
    public static function getListWithPage($where = [], $field = ["a.*"], $page = 1, $limit = 10, $orderBy = "a.id desc")
    {
        // 查询数据
        $query = self::find();
        $query->select($field);
        $query->alias('a');
        $query->where($where);

        $count = $query->count();
        $count = intval($count);
        $offset = ($page - 1) * $limit;
        $query->offset($offset)->limit($limit);

        $list = $query->orderBy($orderBy)->asArray()->all();

        return [
            'count' => $count,
            'list' => $list
        ];
    }

    //不分页查询数据
    public static function getListWithOutPage($where = [], $field = ["a.*"], $orderBy = "a.id desc")
    {
        // 查询数据
        $query = self::find();
        $query->select($field);
        $query->alias('a');
        $query->where($where);
        return $query->orderBy($orderBy)->asArray()->all();
    }

    //查询单个数据
    public static function getOne($where = [], $field = ["a.*"], $orderBy = "a.id desc")
    {
        // 查询数据
        $query = self::find();
        $query->select($field);
        $query->alias('a');
        $query->where($where);
        return $query->orderBy($orderBy)->asArray()->one();
    }


}