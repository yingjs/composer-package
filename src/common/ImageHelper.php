<?php

namespace Yjius\common;
/**
 * php操作图片类方法
 * Class ImageHelper
 * imagejpeg(image, filename) 将图像输出到文件中
 * @package Yjius\common
 * @date  2022/7/7 15:08
 */
class ImageHelper
{

    //图片验证码-验证码
    public static function verifyCode($str = '')
    {
        $image = imagecreate(200, 100);
        imagecolorallocate($image, 0, 0, 0);
        for ($i = 0; $i <= 9; $i++) {
            // 绘制随机的干扰线条
            imageline($image, rand(0, 200), rand(0, 100), rand(0, 200), rand(0, 100), self::rand_color($image));
        }
        for ($i = 0; $i <= 100; $i++) {
            // 绘制随机的干扰点
            imagesetpixel($image, rand(0, 200), rand(0, 100), self::rand_color($image));
        }
        $length = strlen($str);
        if (empty($str)) {
            $length = 4;//验证码长度
            $str = ToolsHelper::random($length);//获取验证码
        }
        $font = 'C:\Windows\Fonts\simhei.ttf';
        for ($i = 0; $i < $length; $i++) {
            // 逐个绘制验证码中的字符
            imagettftext($image, rand(20, 38), rand(0, 60), $i * 50 + 25, rand(30, 70), self::rand_color($image), $font, $str[$i]);
        }
        header('Content-type:image/jpeg');
        imagejpeg($image);
        imagedestroy($image);
    }

    // 生成随机颜色
    private static function rand_color($image)
    {
        return imagecolorallocate($image, rand(127, 255), rand(127, 255), rand(127, 255));
    }

    /**
     * [watermark description]
     * @param  string  $img              [待加水印的图片地址]
     * @param  string  $watermark        [水印图片地址]
     * @param  integer $district         [水印的位置]
     * @param  integer $watermarkquality [图片水印的质量]
     * @return                           [添加水印的图片]
     */
    public static function watermark($img, $watermark, $district = 0,$watermarkquality = 95){
        $imginfo = @getimagesize($img);
        $watermarkinfo = @getimagesize($watermark);
        $img_w = $imginfo[0];
        $img_h = $imginfo[1];
        $watermark_w = $watermarkinfo[0];
        $watermark_h = $watermarkinfo[1];
        if($district == 0) $district = rand(1,9);
        if(!is_int($district) OR 1 > $district OR $district > 9) $district = 9;
        switch($district){
            case 1:
                $x = +5;
                $y = +5;
                break;
            case 2:
                $x = ($img_w - $watermark_w) / 2;
                $y = +5;
                break;
            case 3:
                $x = $img_w - $watermark_w - 5;
                $y = +5;
                break;
            case 4:
                $x = +5;
                $y = ($img_h - $watermark_h) / 2;
                break;
            case 5:
                $x = ($img_w - $watermark_w) / 2;
                $y = ($img_h - $watermark_h) / 2;
                break;
            case 6:
                $x = $img_w - $watermark_w;
                $y = ($img_h - $watermark_h) / 2;
                break;
            case 7:
                $x = +5;
                $y = $img_h - $watermark_h - 5;
                break;
            case 8:
                $x = ($img_w - $watermark_w) / 2;
                $y = $img_h - $watermark_h - 5;
                break;
            case 9:
                $x = $img_w - $watermark_w - 5;
                $y = $img_h - $watermark_h - 5;
                break;
        }
        switch ($imginfo[2]) {
            case 1:
                $im = @imagecreatefromgif($img);
                break;
            case 2:
                $im = @imagecreatefromjpeg($img);
                break;
            case 3:
                $im = @imagecreatefrompng($img);
                break;
        }
        switch ($watermarkinfo[2]) {
            case 1:
                $watermark_logo = @imagecreatefromgif($watermark);
                break;
            case 2:
                $watermark_logo = @imagecreatefromjpeg($watermark);
                break;
            case 3:
                $watermark_logo = @imagecreatefrompng($watermark);
                break;
        }
        if(!$im or !$watermark_logo) return false;
        $dim = @imagecreatetruecolor($img_w, $img_h);
        if(@imagecopy($dim, $im, 0, 0, 0, 0,$img_w,$img_h )){
            imagecopy($dim, $watermark_logo, $x, $y, 0, 0, $watermark_w, $watermark_h);
        }
        $file = dirname($img) . '/w' . basename($img);
        $result = imagejpeg ($dim,$file,$watermarkquality);
        imagedestroy($watermark_logo);
        imagedestroy($dim);
        imagedestroy($im);
        if($result){
            echo $img.' 水印添加成功';
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 等比例缩小输出
     * @param  $file  要缩放的图片路径
     * @param  $width 缩放后的宽度
     * @param  $height缩放后的高度
     * @param  $eq    是否等比缩放
     * @return [type]
     */
    public static function compress($file, $width, $height = '', $eq = true)
    {
        $image = imagecreatefrompng($file);
        $img_info = getimagesize($file);
        if ($eq) $height = $img_info[1] * ($width / $img_info[0]);
        $com_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($com_image, $image, 0, 0, 0, 0, $width, $height, $img_info[0], $img_info[1]);
        header('Content-type:image/jpeg');
        imagejpeg($com_image);
        imagedestroy($com_image);
    }

    /**
     * 剪裁图片
     * @param  $file    需要裁剪的原图
     * @param  $x       裁剪的起始位置的X坐标
     * @param  $y       裁剪的起始位置的Y坐标
     * @param  $width   裁剪的区域的宽度
     * @param  $height  裁剪的区域的高度
     * @return [type]
     */
    public static function cutout($file,$x,$y,$width,$height){
        $image = imagecreatefrompng($file);
        $com_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($com_image, $image, 0, 0, $x, $y, $width, $height, $width, $height);
        header('Content-type:image/jpeg');
        imagejpeg($com_image);
        imagedestroy($com_image);
    }


    /**
     * 首字母头像
     * @param $text
     * @return string
     */
    public static function letterAvatar($text)
    {
        $total = unpack('L', hash('adler32', $text, true))[1];
        $hue = $total % 360;
        list($r, $g, $b) = static::hsv2rgb($hue / 360, 0.3, 0.9);

        $bg = "rgb({$r},{$g},{$b})";
        $color = "#ffffff";
        $first = mb_strtoupper(mb_substr($text, 0, 1));
        $src = base64_encode('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="100" width="100"><rect fill="' . $bg . '" x="0" y="0" width="100" height="100"></rect><text x="50" y="50" font-size="50" text-copy="fast" fill="' . $color . '" text-anchor="middle" text-rights="admin" dominant-baseline="central">' . $first . '</text></svg>');
        $value = 'data:image/svg+xml;base64,' . $src;
//        echo '<img src="' . $value . '"  alt="字母头像" >';exit;
        return $value;
    }

    /**
     * Purpose: 首字母头像用
     * @date 2022/2/24 16:55
     */
    private static function hsv2rgb($h, $s, $v)
    {
        $r = $g = $b = 0;

        $i = floor($h * 6);
        $f = $h * 6 - $i;
        $p = $v * (1 - $s);
        $q = $v * (1 - $f * $s);
        $t = $v * (1 - (1 - $f) * $s);

        switch ($i % 6) {
            case 0:
                $r = $v;
                $g = $t;
                $b = $p;
                break;
            case 1:
                $r = $q;
                $g = $v;
                $b = $p;
                break;
            case 2:
                $r = $p;
                $g = $v;
                $b = $t;
                break;
            case 3:
                $r = $p;
                $g = $q;
                $b = $v;
                break;
            case 4:
                $r = $t;
                $g = $p;
                $b = $v;
                break;
            case 5:
                $r = $v;
                $g = $p;
                $b = $q;
                break;
        }

        return [
            floor($r * 255),
            floor($g * 255),
            floor($b * 255)
        ];
    }


}