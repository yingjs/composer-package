<?php

namespace Yjius\common;

class ArrayHelper
{
    /**
     * 二维数组根据某个key去重
     * @param  [type] $arr [description]
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public static function array2Unique($arr = [], $key = null)
    {
        //建立一个目标数组
        $res = [];
        $existArr = [];
        foreach ($arr as $value) {
            if (!in_array($value[$key], $existArr)) {
                $existArr[] = $value[$key];
                $res[] = $value;
            }
        }
        return $res;
    }


    /**
     * Purpose: 二位数组排序
     * @date 2021/12/3 14:33
     */
    public static function array2Sort($data, $sort_key, $sort_order = SORT_DESC, $sort_type = SORT_NUMERIC)
    {
        if (is_array($data)) {
            foreach ($data as $array) {
                if (is_array($array)) {
                    $key_arrays[] = $array[$sort_key];
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        array_multisort($key_arrays, $sort_order, $sort_type, $data);
        return $data;
    }

}
