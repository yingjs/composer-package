<?php

namespace Yjius\common;

class SecurityHelper
{

    /**
     * 加密字符串
     *
     * @param $string
     * @return string
     */
    public static function Yencrypt($string)
    {
        $keys = array('CI9SsW', 'bxlYmF');
        $agent = substr(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ROBOTS', -5, 5);
        $vala = 0;

        for ($i = 0; $i < strlen($agent); $i++) {
            $vala += ord($agent{$i});
        }

        $vala = substr(strval($vala), -1, 1);
        $valb = (string)random_int(1000, 9999);
        $flag = (strlen($string) % 2) == 1 ? 1 : 0;
        $tmp = $valb . $keys[$flag] . base64_encode($string) . $keys[$flag ^ 1];
        $tmp2 = '';

        for ($i = 0; $i < strlen($tmp) * 2; $i++) {
            if ($i % 2 == 0) {
                $tmp2 .= $valb{$i / 2 % 4};
            } else {
                $tmp2 .= $tmp{($i - 1) / 2};
            }
        }

        $tmp = $valb . base64_encode(strrev($tmp2)) . $flag . $vala;

        return $tmp;

    }

    /**
     * 解密字符串
     *
     * @param $string
     * @return bool|string
     */
    public static function Ydecrypt($string)
    {
        $keys = array('CI9SsW', 'bxlYmF');
        $agent = substr(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'ROBOTS', -5, 5);
        $vala = 0;

        for ($i = 0; $i < strlen($agent); $i++) {
            $vala += ord($agent{$i});
        }

        $vala = substr(strval($vala), -1, 1);
        $valb = substr($string, 0, 4);

        if (substr($string, -1, 1) != $vala) {
            return FALSE;
        }

        $flag = intval(substr($string, -2, 1));
        $tmp = base64_decode(substr($string, 4, strlen($string) - 6));
        $tmp = strrev($tmp);
        $tmp2 = '';

        for ($i = 0; $i < strlen($tmp); $i++) {
            if ($i % 2 == 1) {
                $tmp2 .= $tmp{$i};
            }
        }

        if (
            substr($tmp2, 4, 6) != $keys[$flag]
            || substr($tmp2, -6, 6) != $keys[$flag ^ 1]
            || substr($tmp2, 0, 4) != $valb
        ) {
            return FALSE;
        }

        $tmp2 = base64_decode(substr($tmp2, 10, strlen($tmp2) - 16));

        return $tmp2;
    }

    /**
     * // 密钥必须是32字节长（256位），对于 AES-128 则需要16字节，AES-192 需要24字节
     * $key = substr(hash('sha256', 'your-secret-key-here'), 0, 32);
     * // 初始化向量 (IV) 必须是16字节长，并且每次加密时都应该不同
     * $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
     * $data = '这是要加密的数据';
     *
     * // 加密
     * $encrypted = SecurityHelper::aesEncrypt($data, $key, $iv);
     * echo "Encrypted: " . $encrypted . PHP_EOL;
     * // 解密
     * $decrypted = SecurityHelper::aesDecrypt($encrypted, $key, $iv);
     * echo "Decrypted: " . $decrypted . PHP_EOL;
     * **/

    /**
     * Purpose: AES加密
     * @param $data
     * @param $key
     * @param $iv
     * @return string
     * @author yingjiusheng@gsdata.cn
     * @date 2024/12/23 11:33
     */
    public static function aesEncrypt($data, $key, $iv)
    {
        // 指定加密方法，这里我们选择 AES-256-CBC
        $method = 'aes-256-cbc';

        // 加密数据
        $encryptedData = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);

        // 返回 Base64 编码的加密字符串，以便于存储或传输
        return base64_encode($encryptedData);
    }

    /**
     * Purpose: AES解密
     * @param $encryptedData
     * @param $key
     * @param $iv
     * @return string
     * @author yingjiusheng@gsdata.cn
     * @date 2024/12/23 11:33
     */
    public static function aesDecrypt($encryptedData, $key, $iv)
    {
        // 指定加密方法，这里我们选择 AES-256-CBC
        $method = 'aes-256-cbc';

        // 将 Base64 编码的字符串转换回二进制格式
        $encryptedData = base64_decode($encryptedData);

        // 解密数据
        return openssl_decrypt($encryptedData, $method, $key, OPENSSL_RAW_DATA, $iv);
    }

}
