<?php

namespace Yjius\common;

/**
 * 调试日志操作类
 */
class LoggerHelper
{

    private static $instance = false;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * 初始化调试日志操作类，没有经过初始化的后续调试代码都不会生效
     */
    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new LoggerHelper();
        }
        return self::$instance;
    }

    /**
     * 写入日志文件-结果是json格式
     * @param string $file 文件名
     * @param string $msg 日志内容
     * @param boolean $time 是否记录当前时间
     * @param boolean $ip 是否记录访问者IP
     */
    public static function writeJson($file, $msg, $time = true, $ip = true)
    {
        $info = [];
        $time && $info["time"] = date('Y-m-d H:i:s');
        if ($ip) {
            //部分情况无法获得IP地址，比如用命令行模式操作
            try {
                $userIP = ToolsHelper::getIpAddr();
            } catch (\Exception $e) {
                $userIP = 'UNKNOW|CONSOLE';
            }
            $info["ip"] = $userIP;
        }
        $info["msg"] = $msg;
        $log_path = getenv('LOG_PATH') ?: dirname(dirname(__DIR__)) . '/examples/runtime/log';
        $dir_path = $log_path . '/' . date('Y-m') . '/' . date('d') . '/';
        if (!is_dir($dir_path)) {
            @mkdir($dir_path, 0777, true);
            @chmod($dir_path, 0777);
        }
        if (!file_exists($dir_path . $file)) {
            $flag = 1;//新增的话去修改文件权限
        }
        $infoJson = json_encode($info, JSON_UNESCAPED_UNICODE);
        file_put_contents($dir_path . $file, $infoJson . PHP_EOL, FILE_APPEND);
        if (!empty($flag)) {
            @chmod($dir_path . $file, 0777);
        }
    }

    /**
     * 写入日志信息到指定文件
     *
     * 该方法用于将给定的消息写入到指定的日志文件中，并可以选择性地在消息前附加时间和IP地址
     * 主要用于记录系统或应用程序的运行时信息
     *
     * @param string $file 日志文件名，实际日志将写入到该文件中
     * @param string $msg 要记录的消息内容
     * @param bool $time 是否在消息前附加当前时间，默认为true
     * @param bool $ip 是否在消息前附加当前用户IP地址，默认为true
     */
    public static function write($file, $msg, $time = true, $ip = true)
    {
        // 初始化时间和IP地址字符串
        $timeStr = $ipStr = "";
        // 如果需要附加时间，则生成当前时间字符串
        if ($time) {
            $time = date('Y-m-d H:i:s');
            $timeStr = "【{$time}】";
        }
        // 如果需要附加IP地址，则获取当前用户IP地址
        if ($ip) {
            // 部分情况无法获得IP地址，比如用命令行模式操作
            try {
                $userIP = ToolsHelper::getIpAddr();
            } catch (\Exception $e) {
                $userIP = 'UNKNOW|CONSOLE';
            }
            $ip = $userIP;
            $ipStr = "【{$ip}】";
        }

        // 确定日志文件的存储路径，如果环境变量未设置，则使用默认路径
        $log_path = getenv('LOG_PATH') ?: dirname(dirname(__DIR__)) . '/examples/runtime/log';
        // 根据当前日期构建日志文件的完整目录路径
        $dir_path = $log_path . '/' . date('Y-m') . '/' . date('d') . '/';
        // 检查并创建日志目录，如果目录不存在
        if (!is_dir($dir_path)) {
            @mkdir($dir_path, 0777, true);
            @chmod($dir_path, 0777);
        }
        // 检查日志文件是否存在，如果不存在则设置标志以便后续修改文件权限
        if (!file_exists($dir_path . $file)) {
            $flag = 1;// 新增的话去修改文件权限
        }

        // 将时间和IP地址附加到消息内容前，并写入到日志文件中
        file_put_contents($dir_path . $file, $timeStr . $ipStr . $msg . PHP_EOL, FILE_APPEND);
        // 如果创建了新的日志文件，则修改文件权限为可写
        if (!empty($flag)) {
            @chmod($dir_path . $file, 0777);
        }
    }

}

