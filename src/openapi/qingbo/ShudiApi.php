<?php

namespace Yjius\openapi\qingbo;

use Yjius\common\CurlHelper;

/**
 * 清博属地系统/新媒体系统
 * Class ShudiApi
 * @package common\extend
 * @date  2021/12/6 10:14
 */
class ShudiApi
{
    protected $host = 'http://xmt.gstai.com';
    protected $secret = null;
    protected $company_id = null;

    public function __construct($config = [])
    {
        if (!empty($config['host'])) {
            $this->host = $config['host'];
        }
        if (!empty($config['company_id'])) {
            $this->company_id = $config['company_id'];
        }
        if (!empty($config['secret'])) {
            $this->secret = $config['secret'];
        }
    }

    /**
     * Purpose: 登录属地
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/3/3 9:50
     */
    public function login($userInfo, $company_id = 0)
    {
        if (empty($company_id)) $company_id = $this->company_id;
        if (empty($userInfo['username'])) return ['success' => false, 'msg' => '无用户名信息'];
        $path = "api/manage/login/login";
        $data['company_id'] = $company_id;
        $data['username'] = $userInfo['username'];
        $data['password'] = $userInfo['password'];
        $response = $this->callApi($path, $data);
        return $response;
    }

    /**
     * Purpose: 退出登录
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/3/3 9:51
     */
    public function logout($token)
    {
        if (empty($company_id)) $company_id = $this->company_id;
        $path = "api/manage/sys/logout?token=" . $token;
        $data['company_id'] = $company_id;
        $data['token'] = $token;
        $response = $this->callApi($path, $data, 0);
        return $response;
    }

    /**
     * Purpose: 查询用户列表,按用户名搜索
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/3/2 17:38
     */
    public function getUserList($username = '', $company_id = 0)
    {
        if (empty($company_id)) $company_id = $this->company_id;
        $path = "api/open/get-user-list";
        $response = $this->callApi($path, ['user_name' => $username, 'company_id' => $company_id]);
        return $response;
    }

    /**
     * Purpose: 添加属地用户
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/3/3 9:39
     */
    public function addUser($userInfo)
    {
        if (empty($company_id)) $company_id = $this->company_id;
        if (empty($userInfo['username'])) return ['success' => false, 'msg' => '无用户名信息'];
        $path = "api/open/add-user";
        /*
         * api/open/add-user   添加用户
         * 参数：company_id user_name  password  phone  nickname  is_super(是否管理员1是)
         */
        $data['company_id'] = $company_id;
        $data['user_name'] = $userInfo['username'];
        $data['password'] = $userInfo['username'];
        $data['phone'] = $userInfo['tel'];
        $data['nickname'] = $userInfo['nickname'];
        $data['is_super'] = 0;
        $response = $this->callApi($path, $data);
        return $response;
    }

    /**
     * Purpose: 编辑用户
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/3/3 9:40
     */
    public function editUser($userInfo)
    {
        if (empty($company_id)) $company_id = $this->company_id;
        if (empty($userInfo['username'])) return ['success' => false, 'msg' => '无用户名信息'];
        $path = "api/open/edit-user";
        /*
         * api/open/edit-user  修改用户
         * 参数：company_id user_name  password  phone  nickname  is_super(是否管理员1是)   is_ban(是否禁用 ，1禁用)
         */
        $data['company_id'] = $company_id;
        $data['user_name'] = $userInfo['username'];
        $data['password'] = $userInfo['username'];
        $data['phone'] = $userInfo['tel'];
        $data['nickname'] = $userInfo['nickname'];
        $data['is_super'] = 0;
        $data['is_ban'] = 0;
        $response = $this->callApi($path, $data);
        return $response;
    }

    /**
     * Purpose: 删除用户
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/3/3 9:44
     */
    public function delUser()
    {
        if (empty($company_id)) $company_id = $this->company_id;
        if (empty($userInfo['username'])) return ['success' => false, 'msg' => '无用户名信息'];
        $path = "api/open/del-user";
        /*
         * api/open/del-user     删除用户
         * 参数： company_id user_name
         */
        $data['company_id'] = $company_id;
        $data['user_name'] = $userInfo['username'];
        $response = $this->callApi($path, $data);
        return $response;
    }

    /**
     * 发起请求
     * @param $url
     * @param $params
     */
    private function callApi($url, $params, $isPost = 1, $headers = [])
    {
        $host = $this->host;
        if (empty($host)) {
            return ['error' => '企业域名未配置'];
        }
        $url = $host . "/" . $url;
        //处理sign
        $params['company_id'] = $this->company_id;
        $params['secret'] = $this->secret;
        $sign = $this->generateSign($params);
        $params['sign'] = $sign;
        if (!empty($isPost)) {
            $response = CurlHelper::httpRequest($url, $headers, 'POST', null, $params);
        } else {
            $response = CurlHelper::httpRequest($url, $headers);
        }
        $response = json_decode($response, true);
        if (isset($response['code']) && $response['code'] == 10000) {
            return $response['data'];
        }
        if (isset($response['msg'])) {
            return ['error' => $response['msg']];
        }
        return ['error' => '接口异常'];
    }

    /**
     * Purpose: 签名规则
     * Author:yingjiusheng@gsdata.cn
     * @date 2022/3/22 15:35
     */
    private function generateSign($params)
    {
        ksort($params);
        $paras = '';
        foreach ($params as $key => $value) {
            if ($key != 'sign' && !is_array($value)) {
                $paras .= !$paras ? $key . '=' . $value : '&' . $key . '=' . $value;
            }
        }
        $signature = md5($paras);
        return $signature;
    }
}