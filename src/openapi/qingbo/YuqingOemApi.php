<?php

namespace Yjius\openapi\qingbo;

use Yjius\common\CurlHelper;

/**
 * 清博定制化前台获取oem前台接口数据
 * Class ApiService
 * @package core\services
 * 2022/3/22 14:45
 */
class YuqingOemApi
{

    protected $oemDomain = null;
    protected $apiDomain = "http://yuqingoem.gsdatas.cn:8090";
    protected $secret = null;
    protected $appId = null;

    public function __construct($config = [])
    {
        if (!empty($config['oemDomain'])) {
            $this->oemDomain = $config['oemDomain'];
        }
        if (!empty($config['apiDomain'])) {
            $this->apiDomain = $config['apiDomain'];
        }
        if (!empty($config['secret'])) {
            $this->secret = $config['secret'];
        }
        if (!empty($config['appId'])) {
            $this->appId = $config['appId'];
        }
    }

    /**
     * Purpose: 登录到舆情oem
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/12/6 10:35
     */
    public function login($username, $password)
    {
        $response = $this->callOem("api/user/login/login",
            ['data[username]' => $username, 'data[password]' => $password], 1);
        return $response;
    }

    /**
     * 配置 oemDomain
     * Purpose: 根据oemtoken获取用户信息
     * Author:yingjiusheng@gsdata.cn
     * @date 2021/2/25 15:51
     */
    public function getUserInfoByToken($token)
    {
        $response = $this->callOem("api/custom/user/info-user", [], 0,
            ['Authorization:' . $token]);
        return $response;
    }

    /**********************下列方法使用appid和sercet配置,需单独申请************************/
    /**
     * Purpose: 获取舆情下方案列表
     * Author:yingjiusheng@gsdata.cn
     */
    public function getSchemeList($company_id = 0)
    {
        if (empty($company_id)) {
            $company_id = $this->appId;
        }
        $path = "api/get-company-scheme";
        $response = $this->callOemApi($path, ['company_id' => $company_id]);
        return $response;
    }

    /**
     * 查询单个或者多个方案的数据 id为 整数或者数组
     * @param $id
     */
    public function getScheme($id)
    {
        $path = "api/get-scheme";
        $response = $this->callOemApi($path, ['id' => $id]);
        return $response;
    }

    /**
     * @param $url
     * @param $params
     */
    private function callOemApi($url, $params, $isPost = 1, $headers = [])
    {
        $apiDomain = $this->apiDomain;
        if (empty($apiDomain)) {
            return ['error' => '企业域名未配置'];
        }
        if (empty($this->appId) && empty($this->secret)) {
            return ['error' => '企业域名appId或secret参数缺失'];
        }
        $url = $apiDomain . "/" . $url;
        //处理sign
        $params['app_id'] = $this->appId;
        $sign = $this->generateSign($params);
        $params['sign'] = $sign;
        if (!empty($isPost)) {
            $response = CurlHelper::httpRequest($url, $headers, 'POST', null, $params);
        } else {
            $response = CurlHelper::httpRequest($url, $headers);
        }
        $response = json_decode($response, true);
        if (isset($response['code']) && $response['code'] == 10000) {
            return $response['data'];
        }
        if (isset($response['msg'])) {
            return ['error' => $response['msg']];
        }
        return ['error' => '接口异常'];
    }

    /**
     * @param $url
     * @param $params
     */
    private function callOem($url, $params, $isPost = 1, $headers = [])
    {
        $oemDomain = $this->oemDomain;
        if (empty($oemDomain)) {
            return ['error' => '企业域名未配置'];
        }
        $url = $oemDomain . "/" . $url;
        //处理sign
        $params['app_id'] = $this->appId;
        $sign = $this->generateSign($params);
        $params['sign'] = $sign;
        if (!empty($isPost)) {
            $response = CurlHelper::httpRequest($url, $headers, 'POST', null, $params);
        } else {
            $response = CurlHelper::httpRequest($url, $headers);
        }
        $response = json_decode($response, true);
        if (isset($response['code']) && $response['code'] == 10000) {
            return $response['data'];
        }
        if (isset($response['msg'])) {
            return ['error' => $response['msg']];
        }
        return ['error' => '接口异常'];
    }

    private function generateSign($params)
    {
        $params['secret'] = $this->secret;
        ksort($params);
        $paras = '';
        foreach ($params as $key => $value) {
            if ($key != 'sign' && !is_array($value)) {
                $paras .= !$paras ? $key . '=' . $value : '&' . $key . '=' . $value;
            }
        }
        $signature = md5($paras);
        return $signature;
    }

}