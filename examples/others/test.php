<?php
//计算人事每一层级的人员数量汇总
function countUserTree2($list)
{
    if (empty($list)) {
        return [];
    }
    $count = count(['children']);
    foreach ($list as $key => $val) {
        $list[$key]['children'] = countUserTree2($val['children']);
        if (!empty($list[$key]['children'])) {
            foreach ($list[$key]['children'] as $k => $v) {
                $list[$key]['all'] += $v['all'] + $count;
            }
        }
    }
    return $list;
}

$list = [
    [
        "userid"=> "054658083125908472",
        "name" => "方永康",
        "mobile" => "15255155305",
        "manager_userid" => "040230154035276791",
        "status" => "1",
        "status_i18n" => "全职（正式）",
        "team_tag" => "normal",
        "current" => 2,
        "all" => 0,
        "children" => [
            [
                "userid"=> "175712332630900923",
                "name"=> "程正平",
                "mobile"=> "17775217969",
                "manager_userid"=> "054658083125908472",
                "status"=> "1",
                "status_i18n"=> "全职（正式）",
                "team_tag"=> "normal",
                "current"=> 2,
                "all"=> 0,
                "children" => [
                    [
                        "userid" => "175712332630900923",
                        "name" => "程正平ff",
                        "mobile" => "17775217969",
                        "manager_userid" => "054658083125908472",
                        "status" => "1",
                        "status_i18n" => "全职（正式）",
                        "team_tag" => "normal",
                        "current" => 0,
                        "all" => 0,
                        "children" => [
                            [
                                "userid" => "175712332630900923",
                                "name" => "程正平ff",
                                "mobile" => "17775217969",
                                "manager_userid" => "054658083125908472",
                                "status" => "1",
                                "status_i18n" => "全职（正式）",
                                "team_tag" => "normal",
                                "current" => 0,
                                "all" => 0,
                                "children" => []
                            ],
                        ]
                    ],
                    [
                        "userid" => "175712332630900923",
                        "name" => "程正平ff",
                        "mobile" => "17775217969",
                        "manager_userid" => "054658083125908472",
                        "status" => "1",
                        "status_i18n" => "全职（正式）",
                        "team_tag" => "normal",
                        "current" => 0,
                        "all" => 0,
                        "children" => []
                    ],
                    [
                        "userid" => "175712332630900923",
                        "name" => "程正平ee",
                        "mobile" => "17775217969",
                        "manager_userid" => "054658083125908472",
                        "status" => "1",
                        "status_i18n" => "全职（正式）",
                        "team_tag" => "normal",
                        "current" => 0,
                        "all" => 0,
                        "children" => []
                    ]
                ]
            ],
            [
                "userid"=> "424304476729595513",
                "name"=> "田子龙",
                "mobile"=> "17856441891",
                "manager_userid"=> "054658083125908472",
                "status"=> "1",
                "status_i18n"=> "全职（正式）",
                "team_tag"=> "normal",
                "current"=> 0,
                "all"=> 0,
                "children"=> []
            ]
        ]
    ]
];
$dd = countUserTree2($list);
print_r($dd);exit;