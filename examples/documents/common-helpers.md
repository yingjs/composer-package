## Common-helpers

### 使用场景

- 常用的PHP方法、类封装，可直接初始化使用。

### CurlHelper （封装的curl方法）

#### httpRequest

- http的get、post请求-可见示例

### DateHelper （日期时间处理）

#### offset

- 两个时区间相差的时长,单位为秒-可见示例

#### span

- 计算两个时间戳之间相差的时间-可见示例

#### human

- 格式化 UNIX 时间戳为人易读的字符串-可见示例

#### unixtime

- 获取一个基于时间偏移的Unix时间戳-可见示例
- 最小单位精度为分钟

### Debug（调试-打印）

#### print_r

- 可打印多个值或内容-可见示例

### DebugLog(调试-打印日志)

- 程序运行中打印断点日志- 可见示例

### GPSHelper（经纬度处理转换类）

- BD-09(百度GPS标准)、GCJ-02(中国国家测绘局标准)、WGS-84(全球GPS标准)相互转换
- 应用场景较少，暂无示例

### PinYinHelper（汉字转拼音函数，针对大部分情况）

#### py

- 汉字转拼音-可见示例

### RegexHelper（常用正则表达式）

#### isNames

- 验证用户名-可见示例

#### isPWD

- 验证密码-可见示例

#### isEmail

- 验证邮箱-可见示例

#### isTelephone

- 验证座机号-可见示例

#### isMobile

- 验证手机号-可见示例

#### isPostcode

- 验证邮箱编号-可见示例

#### isIP

- 验证IP地址-可见示例

#### isIDcard

- 验证身份证号-可见示例

#### isURL

- 验证url-可见示例

#### isValid

- 验证自定义正则是否合法-可见示例

### RsaHelper（RSA签名方法）

- 公钥加密私钥解密
- 私钥加密公钥解密

- 可见示例

### SecurityHelper（加密解密方法）

#### Yencrypt

- 加密字符串-可见示例

#### Ydecrypt

- 解密字符串-可见示例

### XssHelper（Xss攻击过滤）

#### xss_clean

- 内容过滤-可见示例

### TreeHelper（通用的树型类）

- 可见示例

### ToolsHelper（常用工具方法）

#### hidTel

- 截取手机号中间四位-可见示例

#### hidIdCard

- 隐藏身份证六位信息-可见示例

#### camelCaseToSwitchData

- 把下划线的数据 转换为 驼峰的数据
- 最多支持二维数组-可见示例

#### switchDataToCamelCase

- 把驼峰的数据 转换为 下划线的数据
- 最多支持二维数组-可见示例

#### getUuid

- 生成一个uuid-可见示例

#### random

- 生成随机数-可见示例

#### cutStr

- PHP截取UTF-8字符串，解决半字符问题
- 截取utf-8字符串，截取后，用 ...代替被截取的部分
- 可见示例

#### subString

- 中文截取，支持gb2312,gbk,utf-8,big5

- 可见示例

#### arraySort

- 二维数组根据某个字段排序
- 可见示例

#### letterAvatar

- 首字母头像图片

- 可见示例

#### formatBytes

- 将字节转换为可读文本

- 可见示例

#### generatePager

- 根据问题列表随机生成num套试卷选项

- 可见示例