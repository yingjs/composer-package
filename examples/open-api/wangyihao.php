<?php
use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子 -已通
    $config = require dirname(__DIR__) . "/config/open-api/wangyihao.php";
    $ser = new \Yjius\openapi\wangyihao\WangYiHaoOauth($config);
    $res = $ser->getCodeUrl();//回调地址必须是微博方设置的管家的地址，
//    $res2 = $ser->getAccessToken("9107fc504fcdbe6ab08b548c3c0eeadc");
    /*
     *    Array
        (
            [access_token] => a462bde8c2fbe5b746a5926d108fd767
            [refresh_token] => 28ceea5f66615d2fcadcd71adcabb90b
            [user_id] => jiusheng_ying@163.com
            [expires_in] => 2022-08-25 17:37:06
        )
     * */
    //网易云官方没有提供获取用户详情的接口，只有user_id。没有第三步
    $res3 = $ser->getWangYiCategory();
    Debug::print_r($res, $res2, $res3);
    exit;
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}