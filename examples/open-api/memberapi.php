<?php
use Yjius\common\Debug;
use Yjius\openapi\qingbo\MemberApi;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    $config = require dirname(__DIR__) . "/config/open-api/memberapi.php";
    $memberapiService = new MemberApi($config);
    //根据手机号查询
    $res1 = $memberapiService->getUserInfoByMobile('18602500579');
    //根据用户名查询
    $res2 = $memberapiService->getUserInfoByUsername('18602500579');
    //根据ID查询
    $res3 = $memberapiService->getUserInfoById('281799');
    //查询用户是否禁用
    $res4 = $memberapiService->forbiddenInfo('281799');
    Debug::print_r($res1, $res2, $res3, $res4);
    exit;
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}