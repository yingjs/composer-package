<?php
use Yjius\common\Debug;
use Yjius\openapi\qingbo\ShudiApi;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    $config = require dirname(__DIR__) . "/config/open-api/shudiapi.php";
    $shudiapiService = new ShudiApi($config);
    $res1 = $shudiapiService->getUserList();
    Debug::print_r($res1);
    exit;
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}