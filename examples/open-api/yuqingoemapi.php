<?php
use Yjius\common\Debug;
use Yjius\openapi\qingbo\YuqingOemApi;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    $config = require dirname(__DIR__) . "/config/open-api/yuqingoemapi.php";
    $yuqingoemapiService = new YuqingOemApi($config);
    //登录oem
    $res1 = $yuqingoemapiService->login('15305652629', '123456');
    //获取用户详情
    $res2 = $yuqingoemapiService->getUserInfoByToken($res1);
    //获取企业下所有的方案ID
    $res3 = $yuqingoemapiService->getSchemeList();
    //获取某个方案详情,可传数组批量查询
    $res4 = $yuqingoemapiService->getScheme([9679, 9864]);
    Debug::print_r($res1, $res2, $res3, $res4);
    exit;
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}