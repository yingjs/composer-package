<?php
use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子 - 没经过测试，使用时请注意
    $config = require dirname(__DIR__) . "/config/open-api/douyin.php";
    $ser = new \Yjius\openapi\douyin\DouYinOauth($config);
    $res = $ser->getCodeUrl();//回调地址必须是微博方设置的管家的地址，
//    $res2 = $ser->getAccessToken("");
//    $res3 = $ser->getLoginInfo();
    Debug::print_r($res, $res2, $res3);
    exit;
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}