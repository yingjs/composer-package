<?php

use Yjius\aibigmodel\AIBigModelBuilder;
use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

$configParams = require dirname(__DIR__) . "/config/aibigmodel/deepseek.php";

try {
    $aiBigModelBuilderInstance = new AIBigModelBuilder($configParams);
    $data = [
        "messages" => [
            [
                'content' => '您是一位资深的舆情分析师',
                'role' => 'system'
            ],
            [
                'content' => "请你评论青少年压力大现象",
                'role' => 'user'
            ]]
    ];
    $reStr = $aiBigModelBuilderInstance->chatOnce($data);
    $re = $aiBigModelBuilderInstance->chatOnceDealResult($reStr);
    Debug::print_r($re);

    exit;

} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
