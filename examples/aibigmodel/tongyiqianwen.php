<?php

use Yjius\aibigmodel\AIBigModelBuilder;
use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

$configParams = require dirname(__DIR__) . "/config/aibigmodel/tongyiqianwen.php";

try {
    $aiBigModelBuilderInstance = new AIBigModelBuilder($configParams);
    $data = [
        "messages" => [
            [
                'content' => '您是一位资深的舆情分析师',
                'role' => 'system'
            ],
            [
                'content' => "请你评论青少年压力大现象",
                'role' => 'user'
            ]]
    ];
    $reStr = $aiBigModelBuilderInstance->sseChatOnce($data, function ($response) {
//        一、 直接输出流式的数据内容
        echo $response;
//         二、处理后输出流式的数据内容
//        $lines = explode("\n", $response);
//        foreach ($lines as $line) {
//            if (strpos($line, 'data:') === 0) {
//                $eventData = substr($line, 5);
//                if ($eventData !== " [DONE]") {
//                    $eventData = json_decode($eventData, true);
//                }
//                echo($eventData['choices'][0]['delta']['content'] ?? '');
//            }
//        }
        // 刷新输出缓冲区，确保数据实时发送给客户端
        ob_flush();
        flush();
    });
    exit();

    $reStr = $aiBigModelBuilderInstance->chatOnce($data);
    $re = $aiBigModelBuilderInstance->chatOnceDealResult($reStr);
    Debug::print_r($reStr, $re);

    exit;

} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
