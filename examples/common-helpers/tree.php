<?php
use Yjius\common\TreeHelper;
use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    $data = [
        ['id' => 1, 'name' => '节点1', 'pid' => 0],
        ['id' => 2, 'name' => '节点2', 'pid' => 0],
        ['id' => 3, 'name' => '子节点1', 'pid' => 1],
        ['id' => 4, 'name' => '子节点1', 'pid' => 1],
        ['id' => 5, 'name' => '子节点1', 'pid' => 2],
        ['id' => 6, 'name' => '子子节点1', 'pid' => 3],
    ];
    TreeHelper::instance()->init($data, 'pid');
    //获取所有子集 要查询的ID
    $treeList = TreeHelper::instance()->getTreeArray(0);
    //按序输出数组
    $treeList2 = TreeHelper::instance()->getTreeList($treeList, 'name');
    //输出菜单样式数据
    $treeList3 = TreeHelper::instance()->getTreeUl(0, "<li value=@id @selected @disabled>@name @childlist</li>");
    //获取所有子级节点数据
    $treeList4 = TreeHelper::instance()->getChildren(1, false);
    //获取下一子级节点数据
    $treeList5 = TreeHelper::instance()->getChild(1);
    //获取所有父级节点数据
    $treeList6 = TreeHelper::instance()->getParents(6, false);
    //获取上一级父级
    $treeList7 = TreeHelper::instance()->getParent(6);

    Debug::print_r($treeList, $treeList2, $treeList3, $treeList4, $treeList5, $treeList6, $treeList7);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}