<?php
use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子

    //默认get请求
    $res1 = Yjius\common\CurlHelper::httpRequest("http://initb.qbtest.com/api/system/index/login", [],
        'GET', ['debug' => '1'], ['username' => 'user_01', 'password' => '111111']);
    //post请求
    $res2 = Yjius\common\CurlHelper::httpRequest("http://initb.qbtest.com/api/system/index/login", [],
        'post', ['debug' => 1], ['username' => 'user_01', 'password' => '111111']);
    //post json 请求
    $request_url = "http://initb.qbtest.com/api/system/index/login?debug=1";
    $data = [
        'username' => 'user_01',
        'password' => '111111',
    ];
    $json_data = json_encode($data, JSON_UNESCAPED_UNICODE);
    $headers = ['Content-Type: application/json', 'Content-Length: ' . strlen($json_data)];
    $res3 = Yjius\common\CurlHelper::httpRequest($request_url, $headers, 'POST', null, $json_data);

    Debug::print_r($res1, $res2, $res3);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}