<?php
use Yjius\common\DebugLog;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //程序运行中打印断点日志- 示例
    //初始化开始类
    DebugLog::init();
    //记录节点运行时间 DebugLog::time("节点1")  仅用于记录运行方法时间
    DebugLog::time("a");
    sleep(1);
    DebugLog::time("b");
    DebugLog::time("c");
    DebugLog::info("one", ['msg' => 'log1']);
    //停止记录日志
    DebugLog::stopLog();
    //记录节点，打印内容运行时间 DebugLog::info("节点1",数组数据)
    DebugLog::info("two", ['msg' => 'log2']);
    DebugLog::info("three", ['msg' => 'log3']);
    //重新开始记录日志
    DebugLog::restartLog();
    DebugLog::info("four", ['msg' => 'log4']);
    DebugLog::info("five", ['msg' => 'log5']);
    //保存info日志到文件
    //设置 DEBUG_LOG_PATH 可自定义日志保存位置
    //  define('DEBUG_LOG_PATH', __DIR__ . '/../runtime/log');
    DebugLog::saveLog();
    //页面直接展示日志结果
    DebugLog::show();

} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
