<?php
use Yjius\common\Debug;
use Yjius\common\XssHelper;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    $xss = new XssHelper();
    $content = 'ddddffd<input onfocus=alert(\'1\') autofocus/>';
    $is_image = false;
    //过滤内容
    $res = $xss->xss_clean($content, $is_image = false);

    echo $res;
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}