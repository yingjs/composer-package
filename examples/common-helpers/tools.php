<?php
use Yjius\common\ToolsHelper;
use Yjius\common\Debug;

//例子
require __DIR__ . "/../../vendor/autoload.php";

try {
    //打包压缩包文件
//    $res = ToolsHelper::zipFiles( ["D:\\htdocs\\test\\Yjius\\examples\\common-helpers\\debug.php",
//        "D:\\htdocs\\test\\Yjius\\examples\\common-helpers\\xss.php"],"D:\\htdocs\\test\\Yjius\\examples\\runtime\\tmp\\new.zip");

    //隐藏中间四位手机号
    $res1 = ToolsHelper::hidTel("18722223333");
    //隐藏身份证号信息
    $res2 = ToolsHelper::hidIdCard("340231199312213232");
    //下划线数组变为驼峰格式
    $res3 = ToolsHelper::humpUnderlineConversion(['user_name' => "admin", ["ab_c" => ["ab_c"=>"ab_c"]]]);
    //驼峰格式变为下划线数组
    $res4 = ToolsHelper::humpUnderlineConversion(['userName' => "admin", ["abC" => 1]],0);
    Debug::print_r($res3,$res4);
    //获取一个getUuid
    $res5 = ToolsHelper::getUuid();
    //生成一个随机字符串
    $res6 = ToolsHelper::random(6);
    //截取utf-8字符串，截取后，用 ...代替被截取的部分
    $res7 = ToolsHelper::cutStr('这是一个待截取的字符串ssss', 10);
    //中文截取，支持gb2312,gbk,utf-8,big5
    $res8 = ToolsHelper::subString('这是一个待截取的字符串ssss', 2, 2);
    //二维数组排序
    $data = [
        'li' => ['score' => 12, 'yu' => 16],
        'li1' => ['score' => 14, 'yu' => 11],
        'li2' => ['score' => 9, 'yu' => 20],
        'li3' => ['score' => 11, 'yu' => 18],
    ];
    $res9 = ToolsHelper::arraySort($data, 'score');
    //首字母头像
    $res10 = ToolsHelper::letterAvatar('应');
    //将字节转换为可读文本
    $res11 = ToolsHelper::formatBytes('112121212');
    //根据选择题，生成试卷题目。随机顺序，随机选项，正确答案
    $data = [
        ["question" => "天空是什么颜色的？", "options" => [['text' => '黑色', "flag" => 0], ['text' => '白色', "flag" => 0], ['text' => '蓝色', "flag" => 1]]],
        ["question" => "大海是什么颜色的？", "options" => [['text' => '黑色', "flag" => 0], ['text' => '白色', "flag" => 0], ['text' => '蓝色', "flag" => 1]]],
        ["question" => "草坪是什么颜色的？", "options" => [['text' => '黑色', "flag" => 0], ['text' => '白色', "flag" => 0], ['text' => '绿色', "flag" => 1]]]
    ];
    $res12 = ToolsHelper::generatePager($data, 2, 1, 1);
    //获取ip
    $res13 = ToolsHelper::getIpAddr();
    $res14 = ToolsHelper::getBrowser();
    $res15 = ToolsHelper::getOs();
    $res16 = ToolsHelper::downloadFileUrl("http://bsddata.oss-cn-hangzhou.aliyuncs.com/teaching_dev/test/20220324153039-623c1e1fc1ffe.png");

    Debug::print_r($res1, $res2, $res3, $res4, $res5, $res6, $res7,
        $res8, $res9, $res10, $res11, $res12,$res13,$res14,$res15);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
