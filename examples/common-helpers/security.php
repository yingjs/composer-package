<?php
use Yjius\common\Debug;
use Yjius\common\SecurityHelper;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //加密方法
    $str = SecurityHelper::Yencrypt("这是加密内容");
    echo "Encrypted: " . $str . PHP_EOL;
    //解密方法
    $res = SecurityHelper::Ydecrypt($str);
    echo "Decrypted: " . $res . PHP_EOL;

    // 密钥必须是32字节长（256位），对于 AES-128 则需要16字节，AES-192 需要24字节
    $key = substr(hash('sha256', 'your-secret-key-here'), 0, 32);
    // 初始化向量 (IV) 必须是16字节长，并且每次加密时都应该不同
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $data = '这是要加密的数据';

    // 加密
    $encrypted = SecurityHelper::aesEncrypt($data, $key, $iv);
    echo "Encrypted: " . $encrypted . PHP_EOL;
    // 解密
    $decrypted = SecurityHelper::aesDecrypt($encrypted, $key, $iv);
    echo "Decrypted: " . $decrypted . PHP_EOL;


} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}