<?php
use Yjius\common\Debug;
use Yjius\common\RegexHelper;

//例子
require __DIR__ . "/../../vendor/autoload.php";

try {
    //验证手机号是否合法、邮箱、身份证等
    $res = RegexHelper::isMobile("18222222222");
    if (!empty($res)) {
        Debug::print_r("合法");
    } else {
        Debug::print_r("不合法");
    }
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
