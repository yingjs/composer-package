<?php
//composer dump-autoload
use Yjius\common\Debug;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    Debug::print_r("打印1", "打印2", ["打印数组1", "打印数组3"]);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
