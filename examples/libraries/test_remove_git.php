<?php

use Yjius\libraries\OperateAfter;

require __DIR__ . "/../../vendor/autoload.php";

try {
    //例子
    $gitDirs = OperateAfter::postInstall();
    \Yjius\common\Debug::print_r($gitDirs);
} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}