<?php
use Yjius\libraries\DbFactory;

require __DIR__ . "/../../vendor/autoload.php";
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';

try {
    //例子
//    https://blog.csdn.net/wuhuagu_wuhuaguo/article/details/80330261

    Yii::setAlias('@common', dirname(__DIR__));
    $rdb = DbFactory::getInstance('mysqldb');
    //查询列表
    $rows = $rdb->createCommand('SELECT * FROM admin_users')->queryAll();
    print_r($rows);
    echo "</br>";

    //查询列表
    $rows = $rdb->createCommand('SELECT * FROM admin_users')->queryOne();
    print_r($rows);
    echo "</br>";

    //更新语句
    $date = date('Y-m-d H:i:s');
    $rows = $rdb->createCommand('update admin_users set update_time="' . $date . '"')->execute();
    print_r($rows);
    echo "</br>";

    //使用常用方式
    $rows = (new \yii\db\Query())
        ->select(['id', 'username'])
        ->from('admin_users')
        ->where(['id' => '1'])
        ->limit(10)
        ->all($rdb);
    print_r($rows);
    echo "</br>";

} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}