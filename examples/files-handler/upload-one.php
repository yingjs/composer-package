<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>文件上传</title>
</head>
<body>
<form method="post" enctype="multipart/form-data" action="/Yjius-php/examples/files-handler/upload-one.php">
    <input name="file" type="file" class='btn'>
    <button type="submit">提交</button>
</form>
</body>
</html>

<?php
use Yjius\fileshandler\FilesHandlerBuilder;
require __DIR__ . "/../../vendor/autoload.php";

$ossParams = require dirname(__DIR__) . "/config/files-handler/oss.php";
$localParams = require dirname(__DIR__) . "/config/files-handler/local.php";
try {

//    uploadOneFile
//    oss    要存储的远程地址，当前文件的临时地址
//    local  要存储的服务器文件名，$FILE内容


    if(!empty($_FILES['file'])){
        $file = $_FILES['file'];
        //LOCAL简单上传
        $uploadBuilder = new FilesHandlerBuilder($localParams);
        $re = $uploadBuilder->uploadOneFile(dirname(dirname(__FILE__)) . '/runtime/tmp/test_local.png', $file);
        print_r($re);
        print_r('<br/>');

        //OSS 简单上传
        $uploadBuilder = new FilesHandlerBuilder($ossParams);
        $re = $uploadBuilder->uploadOneFile('teaching_dev/test/test_local.png',dirname(dirname(__FILE__)) . '/runtime/tmp/test_local.png');
        print_r($re);

        exit;
    }

} catch (Exception $exception) {
    echo $exception->getMessage();
    exit;
}
